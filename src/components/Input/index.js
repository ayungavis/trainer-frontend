import "./styles.scss";

function Input(props) {
  const { id, value, placeholder, label, onChange } = props;
  return (
    <div className="mb-3">
      <label for={id} className="form-label">
        {label}
      </label>
      <input
        type="email"
        className="form-control"
        id={id}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
      />
    </div>
  );
}

export default Input;
