import "./styles.scss";

function Button(props) {
  const { type, variant, children, onClick } = props;

  return (
    <button
      type={type || "button"}
      class={`btn btn-${variant || "primary"}`}
      // class={`btn ${variant ? `btn-${variant}` : 'btn-primary'}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

export default Button;
