import { useState, useEffect } from "react";
import Button from "../Button";

import "./styles.scss";

function Card(props) {
  const { title, content } = props;
  const [counterEffect, setCounterEffect] = useState(content);
  const [counterContent, setCounterContent] = useState(content);

  // Contoh useEffect yang jalan ketika variable counter berubah valuenya
  useEffect(() => {
    // setCounter bakal berjalan ketika variable counter berubah value
    // contohnya jika onClick button di bawah dia mengubah variable counter ke value baru
    // tapi valuenya bakal tetap sama karena setCounter di bawah ini jalan
    setCounterEffect("useEffect yang mengubahnya");
  }, [counterEffect]);

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">{title}</h5>

        <p className="card-text">
          Props content di dalam card: {content}
          <br />
          Variable counterContent di dalam card: {counterContent}
          <br />
          Variable counterEffect di dalam card: {counterEffect}
        </p>
        <Button onClick={() => setCounterContent(1)}>
          Set variable counterContent
        </Button>
        <Button onClick={() => setCounterEffect("onClick yang mengubahnya")}>
          Set variable counterEffect
        </Button>
      </div>
    </div>
  );
}

export default Card;
