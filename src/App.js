import { useState, useEffect } from "react";

import Input from "./components/Input";
import Button from "./components/Button";
import Card from "./components/Card";

import "./App.scss";

function App() {
  const [email, setEmail] = useState();
  const [looping, setLooping] = useState(1);

  // Contoh useEffect componentWillMount (jalan hanya di awal saja)
  useEffect(() => {
    setEmail("emailawal@gmail.com");
  }, []);

  // Contoh useEffect yang looping terus
  useEffect(() => {
    setLooping(looping + 1);
  });

  return (
    <div className="App">
      <header className="App-header">
        <Card title="Title card" content={looping} />
        <p>Variable looping terus: {looping}</p>
        <Input
          label="Contoh input"
          value={email}
          placeholder="johndoe@gmail.com"
          onChange={(e) => setEmail(e.target.value)}
        />
        <Button>Submit</Button>
      </header>
    </div>
  );
}

export default App;
